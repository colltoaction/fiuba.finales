#coding: utf-8

def crawl():
	import requests

	cookies = { 'PHPSESSID' : '6j0bm8l24ka28nemdap5ck0b82' }
	payload = { 'id_cuatrimestre' : 59, # 1° cuatri 2013 \
				'id_departamento' : 61 , # matemática \
				'id_materia' : 102, # álgebra \
				'id_curso' : 194 } # Orecchia
	r = requests.post("http://finales.fi.uba.ar/", data=payload, cookies=cookies)

	# crear carpeta si no existe

	f = open("cuatri1/depto61/mate102/curso194/curso.html", "w")
	f.write(r.text)
	f.close()

def parse(text):
	import html.parser as HP
	class CursoParser(HP.HTMLParser):
		def __init__(self):
			HP.HTMLParser.__init__(self, strict=False)
			self.handling = False
		def handle_starttag(self, tag, attrs):
			if tag == "select":
				name = next((value for (attr, value) in attrs if attr == "name"))
				print("Encountered a select tag with name:", name)
				#if name == "id_":
				self.handling = True
			if self.handling and tag == "option":
				value = next((value for (attr, value) in attrs if attr == "value"))
				print("Encountered an option tag with value:", value)
		def handle_endtag(self, tag):
			if tag == "select":
				self.handling = False
			#~ if self.handling:
				#~ print("Encountered an end tag :", tag)
		def handle_data(self, data):
			if self.handling:
				if not data.isspace():
					print("Encountered some data  :", data)
		
	parser = CursoParser()
	parser.feed(text)


with open("cuatri1/depto61/mate102/curso194/curso.html") as content_file:
    content = content_file.read()

parse(content)
